<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sanbercode | Welcome</title>
</head>
<body>
    <h1>SELAMAT DATANG!</h1>
    <h3>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h3>
    <p>First Name : {{$first_name}}</p>
    <p>Last Name : {{$last_name}}</p>
    <p>Jenis Kelamin : 
        @if($gender == "1") 
            Laki - Laki
        @elseif($gender == "2")
            Perempuan
        @else
            Other
        @endif
    </p>
    <p>Nationality : {{$nationality}}</p>
    <p>Bio : {{$bio}}</p>
</body>
</html>